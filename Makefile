################################################################################
#  Soubor:  Makefile                                                           #
#  Datum:                                                            #
#  Autor:   Marek Pijak, xpijak01@stud.fit.vutbr.cz                            # 
#  Projekt: IPK -                                                              #
#  Zdroj:   maze.cpp                                                      #          
################################################################################

PROJECT=maze

CC=g++
C++FLAGS= -pedantic -Wall -Wextra -lm

all: $(PROJECT)

$(PROJECT): $(PROJECT).cpp
	clear
	$(CC) $(C++FLAGS) -o $(PROJECT) $(PROJECT).cpp
    	
clean:
	rm maze

################################################################################