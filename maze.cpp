#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <fstream>
#include <sstream> 
#include <ctype.h>  
#include <locale.h>
#include <regex.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h> 
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "rapidxml.hpp"
#include "rapidxml_utils.hpp"
#include "rapidxml_print.hpp"


using namespace std;
using namespace rapidxml;
/*
*trida Square--------------------------------------------------------------------
*/

class square
{
  private:
    int top;
    int bottom;
    int left;
    int right;
    int content;
    int guard;
    
  public:
    void set_square (int, int);
    void set_content(int);
    void set_guard(int);
} Square;
  
void square::set_square (int atrr, int num)
{
  
  switch(num){
    case 0:
        top = atrr;
    break;
    case 1:
        bottom = atrr;
    break;
    case 2:
        left = atrr;
    break;
    case 3:
        right = atrr;
    break;  
    case 4:
        content = atrr;
    break;
    case 5:
        guard = atrr;
    break;
  }
  
}

void square::set_content(int contents)
{
    content = contents;
}

void square::set_guard(int id_guard)
{
    guard = id_guard;
}


//Konec square-----------------------------------------------------------------------------------

class game
{
    private:                
        int playerID1;
        int playerID2;
        int playerID3;
        int playerID4;
        vector< vector<square> > map;
    public:
        void set_size(int,int);
        int set_map(char*);
        
}Game;

void game::set_size(int x, int y){

    map.resize(y);
    for (int i = 0; i < y; ++i){
        map[i].resize(x);
    }  

}


int game::set_map(char* fileName){
    int x, y, k;
    rapidxml::file<> xmlFile(fileName); // Default template is char
    rapidxml::xml_document<> doc;

    doc.parse<0>(xmlFile.data());
                                  
    xml_node<> *node = doc.first_node(doc.first_node()->name());
    xml_attribute<> *attr = node->first_attribute();
    x = atoi(attr->value()); 
    
    attr = attr->next_attribute();
    y = atoi(attr->value());  
    
    this->set_size(x,y);
   
    xml_node<> *child = node->first_node();
    for(int i = 0; i < x; i++){
        for(int j = 0; j < y; j++){
            if(child == NULL){
                return 1;
            }   
            k = 0;
            
 
            for (attr = child->first_attribute(); attr; attr = attr->next_attribute()){
               // cout << "name " << attr->name() << " ";
              //  cout << "value " << attr->value() << "\n";
                map[i][j].set_square(atoi(attr->value()), k);
                k++;
            }             
            child = child->next_sibling();
        }
        cout << "\n";
    }
    return 0;
}
                      

/*
*Hrač-----------------------------------------------------------------------------------
*/
class player
{
    public:
            int ID;
            int x;
            int y;
            int rotation;
            bool key;
    public:
            bool go();
            bool turn_left();
            bool turn_right();
            bool stop();
            bool take();
            bool open();
} Player;

bool player::go()
{
   return true;
}

bool player::turn_left()
{
  return true;
}

bool player::turn_right()
{
   return true;
}

bool player::take()
{
  return true;
}

bool player::open()
{
  return true;
}
    



int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        fprintf(stderr, "Path to file is missing\n");    
    } 
    else if(argc == 2)
    {
    
        game* g = new game();  
        
        if(!g->set_map(argv[1])){
            return -1;    
        }
    } 
    else 
    {
        fprintf(stderr, "Too many arguments\n");    
    }
   
   /* int x, y;
    rapidxml::file<> xmlFile("level.xml"); // Default template is char
    rapidxml::xml_document<> doc;

    doc.parse<0>(xmlFile.data());
                                  
    xml_node<> *node = doc.first_node(doc.first_node()->name());
    xml_attribute<> *attr = node->first_attribute();
    x = atoi(attr->value()); 
    
    attr = attr->next_attribute();
    y = atoi(attr->value());  
    
    g->set_size(x,y);
   
    xml_node<> *child = node->first_node();
    for(int i = 0; i < x; i++){
        for(int j = 0; j < y; j++){
            if(child == NULL){
                return 1;
            }   
            for (attr = child->first_attribute(); attr; attr = attr->next_attribute()){
                cout << "name " << attr->name() << " ";
                cout << "value " << attr->value() << "\n";
            }             
            child = child->next_sibling();
        }
        cout << "\n";
    }         */
    
    return 0;
}